<div id="videos-list">
    @foreach($videos as $video)
    <div class="video-item col-xs-12 col-md-10 pull-left panel panel-default">
        <div class="panel-body">
            @if(Storage::disk('images')->has($video->image))
            <div class="video-image-thumb col-md-4 pull-left">
                <div class="video-image-mask">
                    <img src="{{ url('/miniatura/'.$video->image) }}" class="video-image" />
                </div>
            </div>
            @endif
            <div class="data">
                <h3 class="video-title"><a href="{{ route('detailVideo', ['video_id' => $video->id]) }}"> {{$video->title}}</a></h3>
                <p><a href="{{route('channel', ['user_id' => $video->user->id]) }}">{{ $video->user->name.' '. $video->user->surname}}</a> | {{ \FormatTime::LongTimeFilter($video->created_at) }}</p>
            </div>
            <a href="{{ route('detailVideo', ['video_id' => $video->id]) }}" class="btn btn-success">Ver</a>
            @if(Auth::check() && Auth::user()->id == $video->user->id)
            <a href="{{ route('videoEdit', ['video_id' => $video->id]) }}" class="btn btn-warning">Editar</a>
            <a href="" class="btn btn-danger">Eliminar</a>
            @endif

        </div>

    </div>
    @endforeach
    <div class="clearfix"></div>
    {{$videos->links()}}
</div>
