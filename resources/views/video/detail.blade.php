@extends('layouts.app')

@section('content')
<div class="container">
	<div class="col-md-12">
		<h2>{{$video->title}}</h2>
		<hr>
		<div class="col-md-8">
			<!-- Video -->
			<video controls id="video-player">
				<source class="col-md-8" src="{{ route('fileVideo', ['filename' => $video->video_path])}}" >
				Tu navegador no es compatible con HTM5
			</video>
			<!-- Descripcion -->
			<div class="panel panel-default video-data">
				<div class="panel-heading">
					<div class="panel-title">
						Descripción
					</div>
				</div>
				<div class="panel-body">
					Subido po <strong><a href="{{route('channel', ['user_id' => $video->user->id]) }}"> {{$video->user->name.' '.$video->user->surname}} </a></strong>
					| {{ \FormatTime::LongTimeFilter($video->created_at) }}<br>
					{{$video->description}}
				</div>
			</div>


			@include('video.comments')



		</div>
	</div>
</div>
@endsection
