<hr>
<h4>Comentarios</h4>
<hr>

@if(session('message'))
<div class="alert alert-success">
	{{session('message')}}
</div>
@endif

@if(Auth::check())
<div class="row"> 
	<form class="col-md-12" method="POST" action="{{ route('saveComentario') }}">   
		{!! csrf_field() !!}
		<input type="hidden" name="video_id" value="{{$video->id}}" required="">
		<p>Comentario</p>
		<textarea class="form-control" name="body" required="" style="width: 100%;"> </textarea><br>
		<button type="submit" class="btn btn-success pull-right"  >Comentar </button>
	</form>
</div>
<br>
@endif

@if(isset($video->comments))
<div id="comments-list">
	@foreach($video->comments as $comment)

	<div class="panel panel-default video-data">
		<div class="panel-heading">
			<div class="panel-title"> 
				<strong>{{$comment->user->name.' '.$comment->user->surname}}</strong>
				
				@if(Auth::check() && ( Auth::user()->id == $comment->user_id || Auth::user()->id == $video->user_id ))
					<!-- Botón en HTML (lanza el modal en Bootstrap) -->
					<a href="#victorModal" role="button" class="btn btn-sm btn-danger pull-right" data-toggle="modal">Eliminar</a>

					<!-- Modal / Ventana / Overlay en HTML -->
					<div id="victorModal" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title">¿Estás seguro?</h4>
								</div>
								<div class="modal-body">
									<p>¿Seguro que quieres borrar el comentario?</p>
									<p class="text-warning"><small>{{$comment->body}}</small></p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
									<button type="button" class="btn btn-danger">Eliminar</button>
								</div>
							</div>
						</div>
					</div>
				@endif


			</div>
		</div>	
		<div class="panel-body">

			{{ \FormatTime::LongTimeFilter($comment->created_at) }}<br>
			{{$comment->body}}
		</div>
	</div>

	@endforeach
</div>
@endif