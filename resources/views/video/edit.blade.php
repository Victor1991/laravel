@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<h1>Editar Video <b>{{$video->title}}</b></h1>
	</div>
	<hr>
	<form action="{{ route('updateVideo', ['video_id' => $video->id ]) }}" method="post" enctype="multipart/form-data" class="col-lg-7">
		{!! csrf_field() !!}

		@if($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>

			</div>

		@endif
		<div class="form-group">
			<label for="title">Título</label>
			<input type="text" class="form-control" id="title" name="title" value="{{$video->title}}" />
		</div>

		<div class="form-group">
			<label for="description">Descripción</label>
			<textarea class="form-control" id="description" name="description" >{{$video->description}}</textarea>
		</div>

		<div class="form-group">
			<label for="image">Miniatura</label><br>
			 <img style="width: 200px;" src="{{ url('/miniatura/'.$video->image) }}" class="video-image" />
			<input type="file" class="form-control" id="image" name="image" />
		</div>

		<div class="form-group">
			<label for="video">Archivo de vídeo</label><br>
			<video controls id="video-player" style="width: 200px;" >
				<source class="col-md-8" src="{{ route('fileVideo', ['filename' => $video->video_path])}}" >
				Tu navegador no es compatible con HTM5
			</video>
			<input type="file" class="form-control" id="video" name="video" />
		</div>

		<button type="submit" class="btn btn-success"> Modificar Vídeo</button>

	</form>
</div>
@endsection