@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
    	<div class="row">
    		<div class="col-md-4">
			<h1>Busuqeda : {{$search }}</h1>
			</div>
			<div class="col-md-6">
				<form class="pull-right form-inline" action="{{ url('/buscar/'. $search) }}" method="get" style="margin-top: 30px;">
					<label>Ordenar :</label>
					<select class="form-control" name="filter">
						<option value="new">Mas nuevos primero</option>
						<option value="old">Mas antiguos primero</option>
						<option value="alfa">De la A a la Z</option>
					</select>
					<button type="submit" class="btn btn-primary" > Enviar</button>
				</form>
			</div>
		</div>
		<hr class="col-md-10">
		<div class="clearfix"></div>
<br>


		@include('video.videosList')
	</div>
</div>
@endsection

