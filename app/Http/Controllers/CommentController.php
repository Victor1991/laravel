<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFundation\Response;

use App\Video;
use App\Comment;


class CommentController extends Controller
{
	public function saveComment(Request $request) {
 		//Validar formulaio
 		$Validar = $this->validate($request, [
 			'body' => 'required|max:500'
 		]);

 		$Comment = new Comment();
 		$user = \Auth::user();
 		$Comment->user_id = $user->id;
 		$Comment->body =  $request->input('body');
 		$video_id =  $request->input('video_id');
 		$Comment->video_id = $video_id;

 		$Comment->save();

 		return redirect()->route('detailVideo', ['video_id' => $video_id] )->with(array('message' => 'Comentario añadido correctamente !!'));
 	}
  
  }
