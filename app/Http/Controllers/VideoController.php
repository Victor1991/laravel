<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFundation\Response;

use App\Video;
use App\Comment;


class VideoController extends Controller
{
 	public function createVideo(){
		return view('video.createVideo');
 	}

 	public function saveVideo(Request $request) {
 		//Validar formulaio
 		$Validar = $this->validate($request, [
 			'title' => 'required|min:5',
 			'description' => 'required',
 			'video' => 'mimes:mp4'
 		]);

 		$video = new Video();
 		$user = \Auth::user();
 		$video->user_id = $user->id;
 		$video->title =  $request->input('title');
 		$video->description = $request->input('description');

 		//Subida de la miniatura
 		$image = $request->file('image');
 		if ($image) {
 			$image_path = time().$image->getClientOriginalName();
 			\Storage::disk('images')->put($image_path, \File::get($image));

 			$video->image = $image_path;
 		}

 		//Subir Video
 		$video_file = $request->file('video');
 		if ($video_file) {
 			$video_path = time().$video_file->getClientOriginalName();
 			\Storage::disk('videos')->put($video_path, \File::get($video_file));
 			$video->video_path = $video_path;
 		}

 		$video->save();

 		return redirect()->route('home')->with(array('message' => 'El video se ha subido correctamente !!'));
 	}

 	public function getImage($filename) {
 		$file = Storage::disk('images')->get($filename);
 		return $file;
 		//return new Response($file, 200);
 	}

 	public function getVideoDetail($video_id) {
 		$data['video'] = Video::find($video_id);
 		return view('video.detail', $data);
 	}

 	public function getVideo($filename){
 		$file = Storage::disk('videos')->get($filename);
 		return $file;

 	//	return new Response($file, 200);
 	}

 	public function edit($video_id){
 		$user = \Auth::user();
 		$video = Video::findOrFail($video_id);
 		if($user && $video->user_id == $user->id){
 			return view('video.edit', array('video' => $video));
 		}else{
 			return redirect()->route('home');
 		}
 	}

 	public function update($video_id, Request $request){
 		$Validar = $this->validate($request, [
 			'title' => 'required|min:5',
 			'description' => 'required',
 			'video' => 'mimes:mp4'
 		]);
		
		$user = \Auth::user();
 		$video = Video::findOrFail($video_id);
 		$video->user_id = $user->id;
 		$video->title = $request->input('title');
 		$video->description = $request->input('description');

 		//Subida de la miniatura
 		$image = $request->file('image');
 		if ($image) {
 			$image_path = time().$image->getClientOriginalName();
 			\Storage::disk('images')->put($image_path, \File::get($image));

 			$video->image = $image_path;
 		}

 		//Subir Video
 		$video_file = $request->file('video');
 		if ($video_file) {
 			$video_path = time().$video_file->getClientOriginalName();
 			\Storage::disk('videos')->put($video_path, \File::get($video_file));
 			$video->video_path = $video_path;
 		}

 		$video->update();
 		
 		return redirect()->route('home')->with(array('message' => 'El video se ha modificado correctamente !!'));

 	}


 	public function search($search = null , $filter = null) {
 		if (is_null($search)) {
 			$search = \Request::get('search');

 			if (is_null($search)) {
 				return redirect()->route('home');
 			}

 			return redirect()->route('videoSearch', array('search' => $search));	
 		}

 		if (is_null($filter) && \Request::get('filter') && !is_null($search)) {
 			$filter = \Request::get('filter');
 			return redirect()->route('videoSearch', array('search' => $search, 'filter' => $filter ));
 		}

 		$data['search'] = $search;
 		$colum = 'id';
		$order = 'asc';
 		if(!is_null($filter)){
 			switch ($filter) {
 				case 'new':
 					$colum = 'id';
 					$order = 'asc';
 					break;
				case 'old':
 					$colum = 'id';
 					$order = 'desc';
 					break;
 				case 'alfa':
 					$colum = 'title';
 					$order = 'asc';
 					break;
 				default:
 					$colum = 'id';
 					$order = 'asc';
 					break;
 			}
 		}


 		$data['videos'] = Video::where('title', 'LIKE', '%'.$search.'%')->orderBy($colum, $order)->paginate(5);

        return view('video.search', $data);
 	}

 	
}
