<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//use App\Video;

//Route::get('/', function () {

/*	$videos = Video::all();
	foreach ($videos as $key => $video) {
		var_dump($video->title);
		echo"<br>";
		var_dump($video->user->email);
		echo"<br>";
		foreach ($video->comments as $comment) {
			var_dump($comment->body);
		}

		echo"<br><hr>";
	}

	die();*/

  //  return view('welcome');
//});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'HomeController@index')->name('home');


//Rutas de controlado Videos
Route::get('/crear-video', array(
	'as' => 'createVideo',
	'middleware' => 'auth',
	'uses' => 'VideoController@createVideo'
));

Route::post('/guardar-video', array(
	'as' => 'saveVideo',
	'middleware' => 'auth',
	'uses' => 'VideoController@saveVideo'
));

Route::get('/miniatura/{filename}', array(
	'as' => 'imageVide',
	'uses' => 'VideoController@getImage'

 ));

Route::get('/video/{video_id}', array(
		'as' => 'detailVideo',
		'uses' => 'VideoController@getVideoDetail'
	)
);

Route::get('/video-file/{filename}', array(
	'as' => 'fileVideo',
	'uses' => 'VideoController@getVideo'
));

Route::post('/guardar-comentario', array(
	'middleware' => 'auth',
	'as' => 'saveComentario',
	'uses' => 'CommentController@saveComment'
	)
);

Route::get('/editar_video/{video_id}', array(
	'as' => 'videoEdit',
	'middleware' => 'auth',
	'uses' => 'VideoController@edit'
	)
);

Route::post('/update-video/{video_id}', array(
	'as' => 'updateVideo',
	'middleware' => 'auth',
	'uses' => 'VideoController@update'
));



Route::get('/buscar/{search?}/{filter?}', array(
	'as' => 'videoSearch',
	'uses' => 'VideoController@search'
));


//Usuarios
Route::get('/canal/{user_id}', array(
	'as' => 'channel',
	'uses' => 'UserController@channel',
));


// Limpiar Cache
Route::get('/clear-cache', function(){
	$code = Artisan::call('cache:clear');
});
